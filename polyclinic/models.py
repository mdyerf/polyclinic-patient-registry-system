from django.db import models

class Clinic(models.Model):
    name = models.CharField(max_length=20)
    def __str__(self):
        return f'{self.name}'
        
class Patient(models.Model):
    name = models.CharField(max_length=30)
    age = models.IntegerField()
    national_id = models.CharField(max_length=30)
    address = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=15)
 
    def __str__(self):
        return f'{self.name}'

class Admission(models.Model):
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    insurance_type = models.CharField(max_length=20)
    insurance_number = models.CharField(max_length=20)

    STATUS_OPTIONS = [
        ('E', 'entered'),
        ('W', 'waiting'),
        ('D', 'dismissed')
    ]
    DEFAULT_STATUS = 'W'
    status = models.CharField(choices=STATUS_OPTIONS, default=DEFAULT_STATUS, max_length=10)

    patient = models.ForeignKey(Patient, related_name='admissions', on_delete=models.CASCADE)
    clinic = models.ForeignKey(Clinic, related_name='admissions', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.patient.name} - {self.clinic.name}'
